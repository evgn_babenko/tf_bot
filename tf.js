// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http*://backpack.tf/*
// @grant        none
// ==/UserScript==

(function() {
    var hasSpell = function(item) {
        var hasSpell = false;
        for (var prop in item) {
            if (hasSpell) {
                continue;
            }
            if (item.hasOwnProperty(prop)) {
                if (prop.lastIndexOf("spell") !== -1 && item[prop].lastIndexOf("Halloween Spell") !== -1) {
                    hasSpell = true;
                }
            }
        }
        return hasSpell;
    };
    
    //localStorage.removeItem("__result__");
    
    setTimeout(function() {
        console.log('setTimeout');
        $("ul.item-singular > li").each(function(i, el) { 
        var current = $(this).data();
        if (hasSpell(current)) {
            var result = JSON.parse(localStorage.getItem("__result__")) || [];
            result.push( {
                name: current.market_name,
                price: current.p_bptf,
                user: "https://backpack.tf/u/" + expandAccountId(current.listing_account_id)
            });
            localStorage.setItem("__result__", JSON.stringify(result));
            console.log('added');
        }
        $(".fa-angle-right").click();
    });
    }, 1000);
    
})();

// console.table(JSON.parse(localStorage.getItem("__result__")))
// localStorage.removeItem("__result__")